##  Installing and using

1. Create a folder on the local drive and clone the repository ```https://igor_peresunko@bitbucket.org/igor_peresunko/scrapy.git```

2. Navigate to the folder you created that contains a copy of the repository

3. To create a virtual environment ```virtualenv env```

4. Run virtual environment ```. env/bin/activate```

5. Install all packages from file __requirements.txt__ ```pip install -r requirements.txt```

6. Navigate to the folder *../tutorial*

7. To run the spider, use the command ```scrapy crawl <spider> <settings>```
