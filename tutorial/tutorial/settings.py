# -*- coding: utf-8 -*-

BOT_NAME = 'tutorial'

SPIDER_MODULES = ['tutorial.spiders']
NEWSPIDER_MODULE = 'tutorial.spiders'

FEED_URI = 'logs/%(name)s/%(time)s.json'
FEED_FORMAT = 'json'

IMAGES_STORE = 'images/'
IMAGES_EXPIRES = 5

ITEM_PIPELINES = {
	'tutorial.pipelines.HotlineGPU': 1,
    'scrapy.contrib.pipeline.images.ImagesPipeline': None,
    'tutorial.pipelines.TutorialPipeline': None,
}
