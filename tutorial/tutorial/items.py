# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TutorialItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class DmozItem(scrapy.Item):
    title = scrapy.Field()
    link = scrapy.Field()
    desc = scrapy.Field()


class DevelopItem(scrapy.Item):
    name_block = scrapy.Field()
    title = scrapy.Field()
    link = scrapy.Field()


class DjangoItem(scrapy.Item):
    name_section = scrapy.Field()
    description = scrapy.Field()

    topics = scrapy.Field()
    title = scrapy.Field()
    lesson = scrapy.Field()
    url = scrapy.Field()


class PicItem(scrapy.Item):
    image_urls = scrapy.Field()
    title = scrapy.Field()
    url = scrapy.Field()


class BashItem(scrapy.Item):
    hash = scrapy.Field()
    link = scrapy.Field()
    rate = scrapy.Field()
    remark = scrapy.Field()


class PicabuItem(scrapy.Item):
    rating = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()
    tag = scrapy.Field()

    text = scrapy.Field()
    image = scrapy.Field()
    gif = scrapy.Field()
    video = scrapy.Field()


class HotlineItem(scrapy.Item):
    name = scrapy.Field()
    link = scrapy.Field()
    description = scrapy.Field()

    price = scrapy.Field()
    minimal = scrapy.Field()
    medium = scrapy.Field()
    maximum = scrapy.Field()

    features = scrapy.Field()
    manufacturer = scrapy.Field()
    type = scrapy.Field()
    gpu = scrapy.Field()
    memory = scrapy.Field()
    memory_type = scrapy.Field()
    interface = scrapy.Field()
    cooling_system = scrapy.Field()
    work_frequency_gpu = scrapy.Field()
    work_frequency_memory = scrapy.Field()
    memory_bus = scrapy.Field()
    output_connectors = scrapy.Field()
    manufacturers_website = scrapy.Field()

    shops = scrapy.Field()
    name_shop = scrapy.Field()
    link_to_shop = scrapy.Field()
    price_in_shop = scrapy.Field()

    comments = scrapy.Field()
    comm1 = scrapy.Field()
    comm2 = scrapy.Field()
    commentator = scrapy.Field()
    date = scrapy.Field()
    mark = scrapy.Field()
    liked = scrapy.Field()
    dislike = scrapy.Field()
    experience_using = scrapy.Field()


class HotlineBookItem(scrapy.Item):
    name = scrapy.Field()
    link = scrapy.Field()
    author = scrapy.Field()
    description = scrapy.Field()

    price = scrapy.Field()
    minimal = scrapy.Field()
    medium = scrapy.Field()
    maximum = scrapy.Field()

    features = scrapy.Field()
    genre = scrapy.Field()
    year = scrapy.Field()
    publisher = scrapy.Field()
    pages = scrapy.Field()
    isbn = scrapy.Field()


class KinopoiskItem(scrapy.Field):
    name = scrapy.Field()
    link = scrapy.Field()

    features = scrapy.Field()
    year = scrapy.Field()
    country = scrapy.Field()
    director = scrapy.Field()
    genre = scrapy.Field()
    time = scrapy.Field()
    description = scrapy.Field()

    assessment = scrapy.Field()
    rating = scrapy.Field()
    order_top = scrapy.Field()
