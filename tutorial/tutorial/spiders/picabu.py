import scrapy
from scrapy.selector import Selector

from tutorial.items import PicabuItem


class PicabuSpider(scrapy.Spider):
    """Parser for site picabu.ru / all tags"""
    name = 'picabu'
    start_urls = ['http://pikabu.ru/?page=1&twitmode=2', ]

    def parse(self, response):

        for page in range(2, 10):
            a = str(page)
            yield scrapy.Request('http://pikabu.ru/?page=' + a + '&twitmode=2')
            for href in response.xpath('//div[@class="b-story__header-info story_head"]/a[1]/@href'):
                url = response.urljoin(href.extract())
                yield scrapy.Request(url, callback=self.parse_post)

    def parse_post(self, response):
        sel = Selector(response)
        site = sel.xpath('//div[@id="wrap"]')
        post = []

        for i in site:
            item = PicabuItem()
            item['rating'] = i.xpath('//li[@class="b-rating__count"]/text()').extract()
            item['name'] = i.xpath('//div[@class="b-story__header-info story_head"]/a/h1/text()').extract()
            item['link'] = i.xpath('//div[@class="b-story__header-info story_head"]/a[1]/@href').extract()

            image = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/a/img/@src').extract()
            if len(image) != 0:
                item['image'] = image
                item['tag'] = 'image'

            text = i.xpath('//div[@class="b-story__content b-story__content_type_text"]/text()').extract()
            if len(text) != 0:
                item['text'] = text
                item['tag'] = 'text'

            video = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/div[1]/@data-url').extract()
            if len(video) != 0:
                item['video'] = video
                item['tag'] = 'video'

            gif = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/div/a/@href').extract()
            if len(gif) != 0:
                item['gif'] = gif
                item['tag'] = 'gif'

            post.append(item)
        return post
