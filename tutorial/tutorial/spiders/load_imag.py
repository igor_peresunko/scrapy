# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from tutorial.items import PicItem


class LoadImagSpider(CrawlSpider):
    """
    Example for parsing upload pictures site reddit
    """
    name = "load_imag"
    allowed_domains = ["www.reddit.com"]
    start_urls = (
        'https://www.reddit.com/r/pics/',
    )

    rules = [Rule(LinkExtractor(
        allow=['/r/pics/\?count=\d*&after=\w*']),
        callback='parse_item',
        follow=True)
    ]

    def parse_item(self, response):
        selector_list = response.css('div.thing')

        for selector in selector_list:
            item = PicItem()
            item['image_urls'] = selector.xpath('a/@href').extract()
            item['title'] = selector.xpath('div/p/a/text()').extract()
            item['url'] = selector.xpath('a/@href').extract()

            yield item
