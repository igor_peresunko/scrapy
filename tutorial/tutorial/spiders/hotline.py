import scrapy
from scrapy.selector import Selector
from scrapy.spider import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from tutorial.items import HotlineItem


class HotlineSpider(CrawlSpider):
    """Spider for parsing video cards on the website hotline.ua"""
    name = 'hotline'
    allowed_domains = ["hotline.ua"]
    start_urls = ['http://hotline.ua/computer/videokarty/']

    rules = [Rule(LinkExtractor(
        allow=['/computer/videokarty/\?&p=\d*']),
        callback='parse_one',
        follow=True)
    ]

    def parse_one(self, response):
        for href in response.xpath('//ul[@class="catalog  clearfix"]/li/div[3]/div[@class="title-box"]/div[1]/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_product)

    def parse_product(self, response):
        sel = Selector(response)
        sites = sel.xpath('//div[@class="content  full  cf"]')
        product = []

        for site in sites:
            item = HotlineItem()
            price = HotlineItem()
            features = HotlineItem()
            shops = HotlineItem()
            comments = HotlineItem()
            comm1 = HotlineItem()
            comm2 = HotlineItem()

            item['name'] = site.xpath('div[2]/h1/text()').re('[^\s]+')
            item['description'] = site.xpath('//div[@class="opis description"]/p[@class="short-desc"]/text()').extract()
            item['link'] = response.url

            minmax = site.xpath('//a[@data-id="prices"]/strong/text()').re('\d{1,2}\s\d{3}')
            price['minimal'] = minmax[0]
            price['maximum'] = minmax[1]
            item['price'] = price

            features['manufacturer'] = site.xpath('//div[@id="short-props-list"]/table/tr[1]/td/a/text()').extract()
            features['type'] = site.xpath('//div[@id="short-props-list"]/table/tr[2]/td/text()').extract()
            features['gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[3]/td/text()').extract()
            features['memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[4]/td/text()').extract()
            features['memory_type'] = site.xpath('//div[@id="short-props-list"]/table/tr[5]/td/text()').extract()
            features['interface'] = site.xpath('//div[@id="short-props-list"]/table/tr[6]/td/text()').extract()
            features['cooling_system'] = site.xpath('//div[@id="short-props-list"]/table/tr[7]/td/text()').extract()
            features['work_frequency_gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[8]/td/text()').re('(?:\d*\.)?\d+')
            features['work_frequency_memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[9]/td/text()').re('(?:\d*\.)?\d+')
            features['memory_bus'] = site.xpath('//div[@id="short-props-list"]/table/tr[10]/td/text()').re('(?:\d*\.)?\d+')
            features['output_connectors'] = site.xpath('//div[@id="short-props-list"]/table/tr[11]/td/text()').extract()
            features['manufacturers_website'] = site.xpath('//div[@id="short-props-list"]/table/tr[12]/td/a/@href').extract()
            item['features'] = features

            shops['name_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/text()').extract()
            shops['link_to_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/@href').extract()
            shops['price_in_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/td[2]/a/b/text()').re('\d{1,}\s\d{3}')
            item['shops'] = shops

            comm1['commentator'] = site.xpath('//div[@class="th-review"]/ul[1]/li[2]/div[@class="nick"]/a/text()').extract()
            comm1['date'] = site.xpath('//div[@class="th-review"]/ul[1]/li/span[1]/text()').extract()
            comm1['mark'] = site.xpath('//div[@class="th-review"]/ul[1]/li/ul/li/div[1]/b[2]/text()').extract()
            comm1['liked'] = site.xpath('//div[@class="th-review"]/ul[1]/li/ul[@class="use-review"]/li[1]/span/text()').extract()
            comm1['dislike'] = site.xpath('//div[@class="th-review"]/ul[1]/li/ul[@class="use-review"]/li[2]/span/text()').extract()
            comm1['experience_using'] = site.xpath('//div[@class="th-review"]/ul[1]/li/div[@class="user-review-txt"]/p/text()').extract()

            comm2['commentator'] = site.xpath('//div[@class="th-review"]/ul[2]/li[2]/div[@class="nick"]/a/text()').extract()
            comm2['date'] = site.xpath('//div[@class="th-review"]/ul[2]/li/span[1]/text()').extract()
            comm2['mark'] = site.xpath('//div[@class="th-review"]/ul[2]/li/ul/li/div[1]/b[2]/text()').extract()
            comm2['liked'] = site.xpath('//div[@class="th-review"]/ul[2]/li/ul[@class="use-review"]/li[1]/span/text()').extract()
            comm2['dislike'] = site.xpath('//div[@class="th-review"]/ul[2]/li/ul[@class="use-review"]/li[2]/span/text()').extract()
            comm2['experience_using'] = site.xpath('//div[@class="th-review"]/ul[2]/li/div[@class="user-review-txt"]/p/text()').extract()

            comments['comm1'] = comm1
            comments['comm2'] = comm2
            item['comments'] = comments

            product.append(item)

        return product

class TestHotline(scrapy.Spider):
    """Class to test ideas"""
    name = 'testhot'
    allowed_domains = ["hotline.ua"]
    start_urls = ['http://hotline.ua/computer/videokarty/']

    def parse(self, response):
        for href in response.xpath('//ul[@class="catalog  clearfix"]/li/div[3]/div[@class="title-box"]/div[1]/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_product)

    def parse_product(self, response):
        sel = Selector(response)
        sites = sel.xpath('//div[@class="content  full  cf"]')
        product = []

        for site in sites:
            item = HotlineItem()
            price = HotlineItem()
            shops = HotlineItem()
            features = HotlineItem()

            item['name'] = site.xpath('div[2]/h1/text()').re('[^\s]+')
            item['description'] = site.xpath('//p[@class="full-desc"]/text()').extract()
            item['link'] = response.url

            # price['medium'] = site.xpath('//span[@class="prc"]/span/strong/text()').re('\d{1,2}\s\d{3}')
            minmax = site.xpath('//a[@data-id="prices"]/strong/text()').re('\d{1,2}\s\d{3}')
            price['minimal'] = minmax[0]
            price['maximum'] = minmax[1]
            item['price'] = price

            features['manufacturer'] = site.xpath('//div[@id="short-props-list"]/table/tr[1]/td/a/text()').extract()
            features['type'] = site.xpath('//div[@id="short-props-list"]/table/tr[2]/td/text()').extract()
            features['gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[3]/td/text()').extract()
            features['memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[4]/td/text()').extract()
            features['memory_type'] = site.xpath('//div[@id="short-props-list"]/table/tr[5]/td/text()').extract()
            features['interface'] = site.xpath('//div[@id="short-props-list"]/table/tr[6]/td/text()').extract()
            features['cooling_system'] = site.xpath('//div[@id="short-props-list"]/table/tr[7]/td/text()').extract()
            features['work_frequency_gpu'] = site.xpath('//div[@id="short-props-list"]/table/tr[8]/td/text()').re('(?:\d*\.)?\d+')
            features['work_frequency_memory'] = site.xpath('//div[@id="short-props-list"]/table/tr[9]/td/text()').re('(?:\d*\.)?\d+')
            features['memory_bus'] = site.xpath('//div[@id="short-props-list"]/table/tr[10]/td/text()').re('(?:\d*\.)?\d+')
            features['output_connectors'] = site.xpath('//div[@id="short-props-list"]/table/tr[11]/td/text()').extract()
            features['manufacturers_website'] = site.xpath('//div[@id="short-props-list"]/table/tr[12]/td/a/@href').extract()
            item['features'] = features

            shops['name_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/text()').extract()
            shops['link_to_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/th[1]/p/a/@href').extract()
            shops['price_in_shop'] = site.xpath('//div[@class="th-prices"]/table/tr/td[2]/a/b/text()').re('\d{1,}\s\d{3}')
            item['shops'] = shops

            product.append(item)

        return product
