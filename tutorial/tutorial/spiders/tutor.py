# -*- coding: utf-8 -*-
import scrapy
import logging
from scrapy.selector import Selector

from tutorial.items import DmozItem, DevelopItem, DjangoItem, BashItem


class TutorialSpider(scrapy.Spider):
    """
    Parser for Development Tutorials by Robert Thorpe
    """
    name = "dev_tutorials"
    allowed_domains = ["development-tutorials.zeef.com"]
    start_urls = [
        "https://development-tutorials.zeef.com/robert.thorpe",
    ]

    def parse(self, response):
        sel = Selector(response)
        sites = sel.xpath('//div[@id="blocks"]/div')
        items = []

        for i in sites:
            item = DevelopItem()
            item['name_block'] = i.xpath('form[1]/h3/span[2]/span/text()').re('[^\n\t]+')
            item['title'] = i.xpath('form[2]/ol/li/a/span[2]/span/text()').extract()
            item['link'] = i.xpath('form[2]/ol/li/a[1]/@href').extract()
            items.append(item)

        return items


class CopyBodySpider(scrapy.Spider):
    """
    Parser to copy in the markup file of the site
    """
    name = "copy"
    start_urls = [
        "http://hotline.ua/computer-videokarty/msi-gtx-970-gaming-4g/",
    ]

    def parse(self, response):
        filename = response.url.split("/")[-2] + '.html'
        with open(filename, 'wb') as f:
            f.write(response.body)


class TestSpider(scrapy.Spider):
    """
    Test parser (for example)
    """
    name = "test"
    allowed_domains = ["dmoz.org"]
    start_urls = [
        "http://www.dmoz.org/Computers/Programming/Languages/Python/Books/",
        "http://www.dmoz.org/Computers/Programming/Languages/Python/Resources/",
    ]

    def parse(self, response):
        sel = Selector(response)
        sites = sel.xpath('//ul[@class="directory-url"]/li')
        items = []

        for site in sites:
            item = DmozItem()
            item['title'] = site.xpath('a/text()').extract()
            item['link'] = site.xpath('a/@href').extract()
            item['desc'] = site.xpath('text()').re('-\s[^\n]*\\r')
            items.append(item)

        return items


class DjangoSpider(scrapy.Spider):
    """
    Parser documentation for Django
    """
    name = "django"
    start_urls = ["https://docs.djangoproject.com/en/1.8/"]

    def parse(self, response):
        sel = Selector(response)
        site = sel.xpath('//div[@id="s-django-documentation"]/div')
        sections = []

        for i in site:
            item = DjangoItem()
            item2 = DjangoItem()

            item['name_section'] = i.xpath('h2/text()').extract()
            item['description'] = i.xpath('p/text()').extract()

            item2['title'] = i.xpath('ul/li/strong/text()').re('[^:]+')
            item2['lesson'] = i.xpath('ul/li/a/em/text()').extract()
            item2['url'] = i.xpath('ul/li/a/@href').extract()

            item['topics'] = item2
            sections.append(item)
        return sections


class BashSpider(scrapy.Spider):
    name = 'bash'
    start_urls = ['http://bash.im/index/1068']

    def parse(self, response):
        sel = Selector(response)
        site = sel.xpath('//div[@class="quote"]/div[@class="actions"]')
        joke = []

        for i in site:
            item = BashItem()
            item['hash'] = i.xpath('a[@class="id"]/text()').extract()
            item['link'] = i.xpath('a[@class="id"]/@href').extract()
            item['rate'] = i.xpath('span[1]/span/text()').extract()

            joke.append(item)
        return joke
