import scrapy
from scrapy.selector import Selector
from scrapy.spider import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from tutorial.items import HotlineBookItem


class HotlineBookSpider(CrawlSpider):
    """Parser for the programming books hotline.ua"""
    name = 'hotline_book'
    start_urls = ['http://hotline.ua/knigi/programmirovanie/', ]

    rules = [Rule(LinkExtractor(
        allow=['knigi/programmirovanie/\?page=\d*',
               'kompyuter/programmirovanie/\?page=\d*']),
        callback='parse_one',
        follow=True)
    ]

    def parse_one(self, response):
        for href in response.xpath('//div[@class="books one"]/ul/li/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_book)

    def parse_book(self, response):
        sel = Selector(response)
        book = sel.xpath('//div[@class="elast"]')
        product = []

        for i in book:
            item = HotlineBookItem()
            features = HotlineBookItem()
            price = HotlineBookItem()

            item['name'] = i.xpath('//h1[@class="title-main"]/text()').extract()
            item['link'] = response.url
            item['author'] = i.xpath('//div[@class="tovar top"]/div[1]/a/text()').extract()
            item['description'] = i.xpath('//p[@class="short-desc"]/text()').extract()

            minmax = i.xpath('//div[@class="box"]/span/text()').re('\d*\s*(\d{3})')
            price['minimal'] = minmax[0]
            price['medium'] = i.xpath('//div[@class="box"]/span/span/strong/text()').re('\d*\s*(\d{3})')
            price['maximum'] = minmax[1]
            item['price'] = price

            features['genre'] = i.xpath('//div[@class="th-tabl"]/table/tr[1]/td/a/text()').extract()
            features['year'] = i.xpath('//div[@class="th-tabl"]/table/tr[2]/td/text()').extract()
            features['publisher'] = i.xpath('//div[@class="th-tabl"]/table/tr[3]/td/text()').extract()
            features['pages'] = i.xpath('//div[@class="th-tabl"]/table/tr[4]/td/text()').extract()
            features['isbn'] = i.xpath('//div[@class="th-tabl"]/table/tr[5]/td/text()').extract()
            item['features'] = features

            product.append(item)
        return product
