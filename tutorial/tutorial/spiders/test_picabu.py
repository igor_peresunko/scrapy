import scrapy
from scrapy.selector import Selector

from tutorial.items import PicabuItem


class PicabuTextSpider(scrapy.Spider):
    """Parser for site picabu.ru / parser tag text"""
    name = 'picabu_text'
    start_urls = ['http://pikabu.ru/tag/%D1%82%D0%B5%D0%BA%D1%81%D1%82/hot?page=1&twitmode=2']

    def parse(self, response):

        for page in range(2, 11):
            a = str(page)
            yield scrapy.Request('http://pikabu.ru/tag/%D1%82%D0%B5%D0%BA%D1%81%D1%82/hot?page=' + a + '&twitmode=2')
            for href in response.xpath('//div[@class="b-story__header-info story_head"]/a[1]/@href'):
                url = response.urljoin(href.extract())
                yield scrapy.Request(url, callback=self.parse_post)

    def parse_post(self, response):
        sel = Selector(response)
        site = sel.xpath('//div[@id="wrap"]')
        post = []

        for i in site:
            item = PicabuItem()
            item['rating'] = i.xpath('//li[@class="b-rating__count"]/text()').extract()
            item['name'] = i.xpath('//div[@class="b-story__header-info story_head"]/a/h1/text()').extract()
            item['link'] = i.xpath('//div[@class="b-story__header-info story_head"]/a[1]/@href').extract()
            item['text'] = i.xpath('//div[@class="b-story__content b-story__content_type_text"]/text()').extract()
            post.append(item)
        return post


class PicabuTestSpider(scrapy.Spider):
    """ Parser for testing ideas"""
    name = 'picabu_test'
    start_urls = ['http://pikabu.ru/story/otlichnaya_reklama_saytu_poluchilas_3633029',
                  'http://pikabu.ru/story/lyublyu_smotret_na_tebya_moya_tsyipa_3633197',
                  'http://pikabu.ru/story/sezon_osennego_obostreniya_nachalsya_3632852',
                  'http://pikabu.ru/story/krasivo_i_vkusno_3632317']

    def parse(self, response):
        sel = Selector(response)
        site = sel.xpath('//div[@id="wrap"]')
        post = []

        for i in site:
            item = PicabuItem()
            item['rating'] = i.xpath('//li[@class="b-rating__count"]/text()').extract()
            item['name'] = i.xpath('//div[@class="b-story__header-info story_head"]/a/h1/text()').extract()
            item['link'] = i.xpath('//div[@class="b-story__header-info story_head"]/a[1]/@href').extract()

            image = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/a/img/@src').extract()
            if len(image) != 0:
                item['image'] = image
                item['tag'] = 'image'

            text = i.xpath('//div[@class="b-story__content b-story__content_type_text"]/text()').extract()
            if len(text) != 0:
                item['text'] = text
                item['tag'] = 'text'

            video = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/div[1]/@data-url').extract()
            if len(video) != 0:
                item['video'] = video
                item['tag'] = 'video'

            gif = i.xpath('//div[@class="b-story__content b-story__content_type_media"]/div/a/@href').extract()
            if len(gif) != 0:
                item['gif'] = gif
                item['tag'] = 'gif'

            post.append(item)
        # stats = self.crawler.stats.get_stats()  # statistics and write to the file
        # post.append(stats)
        return post
