import scrapy
from scrapy.selector import Selector

from tutorial.items import KinopoiskItem


class KinopoiskSpider(scrapy.Spider):
    """Parser for the top 250 movies on kinopoisk.ru"""
    name = 'kinopoisk'
    start_urls = ['http://www.kinopoisk.ru/top/', ]

    def parse(self, response):
        for href in response.xpath('//div[@class="block_left"]/table[1]/tr/td/table[3]/tr/td/table/tr/td[2]/a/@href'):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_film)

    def parse_film(self, response):
        sel = Selector(response)
        film = sel.xpath('//div[@id="content_block"]')
        films = []

        for i in film:
            item = KinopoiskItem()
            features = KinopoiskItem()
            assessment = KinopoiskItem()

            item['name'] = i.xpath('//h1[@class="moviename-big"]/text()').extract()
            item['link'] = response.url

            features['year'] = i.xpath('//table[@class="info"]/tr[1]/td[2]/div/a/text()').extract()
            features['country'] = i.xpath('//table[@class="info"]/tr[2]/td[2]/div/a/text()').extract()
            features['director'] = i.xpath('//table[@class="info"]/tr[4]/td[2]/a/text()').extract()
            features['genre'] = i.xpath('//table[@class="info"]/tr[11]/td[2]/span/a/text()').extract()
            features['time'] = i.xpath('//table[@class="info"]/tr[21]/td[2]/text()').extract()
            features['description'] = i.xpath('//div[@class="brand_words"]/text()').extract()
            item['features'] = features

            assessment['rating'] = i.xpath('//span[@class="rating_ball"]/text()').extract()
            assessment['order_top'] = i.xpath('//div[@class="div1"]/span/a/text()').extract()
            item['assessment'] = assessment

            films.append(item)
        return films
