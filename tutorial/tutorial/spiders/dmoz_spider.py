import scrapy

from tutorial.items import DmozItem


class DmozSpider(scrapy.Spider):
    name = "dmoz"
    allowed_domains = ["dmoz.org"]
    start_urls = [
        "http://www.dmoz.org/Computers/Programming/Languages/Python/",
    ]

    def parse(self, response):
        for href in response.css("ul.directory.dir-col > li > a::attr('href')"):
            url = response.urljoin(href.extract())
            yield scrapy.Request(url, callback=self.parse_dir_contents)

    def parse_dir_contents(self, response):
        for sel in response.xpath('//ul/li'):
            item = DmozItem()
            item['title'] = sel.xpath('a/text()').extract()
            item['link'] = sel.xpath('a/@href').extract()
            item['desc'] = sel.xpath('text()').extract()
            yield item


class HabrSpider(scrapy.Spider):
    name = 'habr_titles'
    start_urls = ["http://habrahabr.ru/"]

    def parse(self, response):
        for sel in response.xpath('//div[@class="post shortcuts_item"]'):
            item = DmozItem()
            item['title'] = sel.xpath('h1/a/text()').extract()
            item['link'] = sel.xpath('h1/a/@href').extract()
            yield item


class RestSpider(scrapy.Spider):
    name = 'rest'
    start_urls = ["http://www.django-rest-framework.org/"]

    def parse(self, response):
        for sel in response.xpath('//div[@id="table-of-contents"]/ul'):
            item = DmozItem()
            item['title'] = sel.xpath('li/a/text()').extract()
            item['link'] = sel.xpath('li/a/@href').extract()
            yield item


class HabrTopSpider(scrapy.Spider):
    name = 'top_habr'
    start_urls = ["http://habrahabr.ru/"]

    def parse(self, response):
        for sel in response.xpath('//div[@class="block daily_best_posts"]/div[@class="posts_list"]'):
            item = DmozItem()
            item['title'] = sel.xpath('div[@class="post_item"]/a[1]/text()').extract()
            item['link'] = sel.xpath('div[@class="post_item"]/a[1]/@href').extract()
            yield item
